import time
import requests
import json
import datetime
from time import gmtime, strftime
import sys


#input
today = str(datetime.date.today()) # dateStarted in payload can not be empty
usr = sys.argv[3]                  # Jira User Name
comment = sys.argv[4]              # worklog description between ""
issue = str(sys.argv[2])           # issue key 
seconds = int(sys.argv[5]) * 60    # worked time in minutes parsed to seconds for payload

#payload
headers = {
    'Cookie': 'JSESSIONID=' + sys.argv[1],
    'Content-Type': 'application/json'
}

data = {
   "timeSpentSeconds":seconds,
   "dateStarted":today,
   "comment": comment,
   "author":{
      "name": usr
   },
   "issue":{
      "key": issue
   }
}

#API call
response = requests.post('https://jira.uitdatabank.be/rest/tempo-timesheets/3/worklogs/?tempoApiToken=b17538af-34ed-4dab-8883-c810fb651233', headers= headers, data = json.dumps(data))
reply = response.text
print(reply)

# inspiration for further dev:
#https://community.atlassian.com/t5/Answers-Developer-Questions/using-tempo-API-to-add-worklog-in-JIRA-on-behalf-of-someone-else/qaq-p/496055
#https://pimylifeup.com/raspberry-pi-light-sensor/
# counting seconds: https://codereview.stackexchange.com/questions/26534/is-there-a-better-way-to-count-seconds-in-python
